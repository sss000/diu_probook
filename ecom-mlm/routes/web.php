<?php

use Illuminate\Support\Facades\Route;

Route::group(["namespace"=>"Backend"],function() {
    Route::group(['middleware' =>'guest'], function () {
        Route::get('/admin', 'LoginController@login_index')->name('login');
        Route::get('/register', 'LoginController@register_index')->name('register');
        Route::post('/register-store', 'LoginController@store')->name('user.store');
        Route::post('/login', 'LoginController@login')->name('user.login');
        Route::get('/verify/{token}', 'LoginController@user_verify')->name('verify');

         //login with google account
         Route::get('login/google', 'LoginController@redirect');
         Route::get('callback/google', 'LoginController@callback');


         //login with facebook account
         Route::get('/login/facebook', 'LoginController@redirectToProvider');
         Route::get('/login/facebook/callback', 'LoginController@handleProviderCallback');

    });


    Route::group(['middleware' => ['auth','user.role']], function () {
        Route::get('/logout', 'LoginController@logout')->name('logout');
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        Route::get('level', 'ShareHolderLevelController@index')->name('level');
        Route::post('level-store', 'ShareHolderLevelController@store')->name('level.store');
        Route::post('level-update', 'ShareHolderLevelController@update')->name('level.update');

        Route::get('user-list', 'DashboardController@user_list')->name('user.list');
        Route::post('update-user', 'loginController@update')->name('update.user');
        Route::post('delete-user', 'loginController@destroy')->name('delete.user');
        Route::get('user-role', 'RoleController@create')->name('user.role');
        Route::post('user-role-store', 'RoleController@store')->name('store.role');
        Route::get('role-edit/{id}', 'RoleController@edit')->name('role.edit');
        Route::post('role-update', 'RoleController@update')->name('update.role');
        Route::post('role-delete/{id}', 'RoleController@destroy')->name('role.delete');
        //banar
        Route::get('banar-list', 'BanarController@index')->name('banar.list');
        Route::get('banar', 'BanarController@index_banar')->name('banar');
        Route::post('banar-update', 'BanarController@update')->name('banar.update');
        Route::post('banar-upload', 'BanarController@upload')->name('banar.upload');
        Route::post('delete-banar', 'BanarController@delete')->name('banar.delete');
        //category
        Route::get('categories', 'CategoryController@index')->name('categories');
        Route::post('create-category', 'CategoryController@store')->name('category.add');
        Route::post('update-category', 'CategoryController@update')->name('category.update');
        Route::post('delete-category', 'CategoryController@destroy')->name('category.delete');
        Route::post('active-category', 'CategoryController@active')->name('category.active');
        Route::post('inactive-category', 'CategoryController@inactive')->name('category.inactive');
        //sub cat
        Route::get('sub-categories', 'ChildCategoryController@index')->name('child.category');
        Route::post('sub-category-create', 'ChildCategoryController@store')->name('child.add');
        Route::post('sub-category-update', 'ChildCategoryController@update')->name('update.child');
        Route::post('sub-category-delete/{id}', 'ChildCategoryController@destroy')->name('delete.child');
        //sub sub-cat
        Route::get('sub-sub-categories', 'SubChildCategoryController@index')->name('sub.child.category');
        Route::post('sub-sub-category-create', 'SubChildCategoryController@store')->name('sub.child.add');
        Route::post('sub-sub-category-update', 'SubChildCategoryController@update')->name('update.sub.child');
        Route::post('sub-sub-category-delete/{id}', 'SubChildCategoryController@destroy')->name('delete.sub.child');

        //product attribute route
        Route::get('product-attributes', 'AttributeController@index')->name('attributes');
        Route::post('product-attribute-update', 'AttributeController@update')->name('update.attribute');
        Route::post('product-attribute-create', 'AttributeController@store')->name('store.attribute');

        //attr value route
        Route::post('product-attribute-value-create', 'AttributeValueController@store')->name('store.attribute.value');
        Route::post('product-attribute-value-update', 'AttributeValueController@update')->name('update.attribute.value');


         //Brand
        //  Route::get('products', 'ProductController@index')->name('products');
        Route::post('brand-create', 'BrandController@store')->name('brand.add');
        Route::get('brand-list', 'BrandController@index')->name('brand.brand_list');
         Route::post('get-cat-subCat', 'BrandController@getCatSubCat')->name('get.cat.subCat');
         Route::post('brand-update', 'BrandController@update')->name('brand.update');

        //product
        Route::get('products', 'ProductController@index')->name('products');
        Route::post('product-create', 'ProductController@store')->name('product.add');
        Route::get('product-edit/{slug}', 'ProductController@edit')->name('product.edit');
        Route::post('product-update/{slug}', 'ProductController@update')->name('product.update');
        Route::post('product-delete/{id}', 'ProductController@destroy')->name('product.delete');
        Route::post('product-flash-update', 'ProductController@flash_update')->name('product.flash.update');
        Route::post('product-status', 'ProductController@product_status')->name('product.status');

        //product avatars
        Route::get('products/avatars', 'ProductAvatarController@index')->name('avatars');
        Route::get('products/avatars/{slug}', 'ProductAvatarController@show')->name('product.avatars');
        Route::post('product-avatar-create', 'ProductAvatarController@store')->name('avatar.upload');
        Route::post('product-avatar-update', 'ProductAvatarController@update')->name('avatar.update');

         //ads manager
        Route::get('ads', 'AdManagerController@index')->name('ads');
        Route::get('all-ads', 'AdManagerController@get_ads')->name('ads-all');
        Route::post('update-ads', 'AdManagerController@update')->name('ads.update');
        Route::post('ads-create', 'AdManagerController@store')->name('ads.upload');
        // Route::post('product-avatar-update', 'ProductAvatarController@update')->name('avatar.update');
        Route::post('ads-delete/{id}', 'AdManagerController@destroy')->name('ads.delete');

        //sales owner
        Route::get('product-sales-history', 'OrdersController@sales_history')->name('sales.history');

        //product delivery
        Route::post('product-delivered', 'OrdersController@delivery')->name('product.delivery');

        //orders
        Route::post('order/{id}', 'OrdersController@delete_single_order')->name('single.order.delete');
        Route::get('order-refunds', 'OrdersController@refundView')->name('refund.view');
        Route::post('order-refunded', 'OrdersController@refunded')->name('product.refunded');

        //invoice
        Route::post('order-invoice', 'OrdersController@invoice')->name('order.invoice');

        //table data search daily,weekly,monthly,yearly
        Route::post('product-sales-history', 'ProductController@table_search')->name('table-search');


        //Subscription
        Route::get('subscribers', 'SubscriberController@index')->name('subscribers');
        Route::get('update-subscriber-status/{id}/{status}', 'SubscriberController@updateSubscriberStatus');
        Route::get('delete-subscriber/{id}', 'SubscriberController@deleteSubscriber');
        Route::get('export-subscriber-list','SubscriberController@exportSubscriber');


        //Settings
        Route::post('save-settings', 'SettingsController@store')->name('settings.save');
        Route::get('setup-settings', 'SettingsController@index')->name('setup-settings');

    });

});


Route::group(["namespace"=>"Frontend"],function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/category/{slug}', 'HomeController@category')->name('category');
    Route::post('category/load-category', 'HomeController@load_category');
    Route::get('/{user}/wishlist', 'WishListController@index')->name('wishlist');
    Route::post('wishlist/store', 'WishListController@store')->name('wishlist.store');
    Route::post('{params?}/wishlist/store', 'WishListController@store');
    Route::post('/wishlist/delete/{id}', 'WishListController@destroy')->name('wishlist.delete');
    Route::get('/{name}/cart', 'CartController@index')->name('cart');
    Route::post('/cart/store', 'CartController@store')->name('cart.store');
    Route::post('{params?}/cart/store', 'CartController@store');
    // Route::get('/cart/billing-address', 'CartController@billing_index')->name('cart.bill');
    Route::post('/cart/update', 'CartController@update')->name('cart.update');
    Route::post('/cart/item/delete', 'CartController@destroy')->name('cart.item.delete');

    Route::post('product-shipp-update', 'HomeController@updateProductShipp')->name('product.shipp.des');


    Route::get('/user/logout', 'UserController@logout')->name('user.logout');
    Route::get('/{user}/profile', 'UserController@index')->name('user');
    Route::get('/view/{slug}', 'HomeController@product_quick_view')->name('product.quick');
    Route::get('/quick/view/{slug}', 'HomeController@quick_view')->name('quick');

    //order refund
    Route::post('order-refund', 'HomeController@refund')->name('product.refund');
    //search product
    Route::post('search', 'HomeController@search');
    Route::post('{optional?}/search', 'HomeController@search');
    Route::post('{optional?}/{param?}/search', 'HomeController@search');
    //
    // Route::get('search-result/{search}', 'HomeController@search_result')->name("search");
    // Route::get('{parm?}/search-result/{search}', 'HomeController@search_res')->name("search");
    // Route::get('{parm?}/{param1?}/search-result/{search}', 'HomeController@search_r')->name("search");
    // Route::get('{parm?}/{param1?}/{param2?}/search-result/{search}', 'HomeController@search_re')->name("search");
    //
    Route::post('{params?}/search-data', 'HomeController@get_result');
    Route::post('{params?}/{par?}/search-data', 'HomeController@get_result');
    Route::post('{params?}/{par?}/{param1?}/search-data', 'HomeController@get_result');
    // Route::get('search-result/search-product-by-brand/{id}', 'HomeController@search_product_by_brand');
    Route::post('load/{item}', 'HomeController@load')->name('load');


    //Subscriber
    Route::post('check-subscriber-email','NewsletterController@checkSubscriber');
    Route::post('add-subscriber','NewsletterController@addSubscriber');






    Route::get('login', 'UserController@index');

    Route::get('user-register', 'UserController@registerIndex');

    Route::get('cart', 'HomeController@viewCart');

    Route::get('/cart/billing-address', 'HomeController@billing_index')->name('cart.bill');

    Route::get('confirm_pay', 'HomeController@confirmPayment');

    Route::get('wishlist', 'HomeController@viewWishlist');

    Route::get('product-details', 'HomeController@productDetails');

    Route::get('{slug}', 'HomeController@productByCategory')->name('category.product');


});

// SSLCOMMERZ Start
Route::get('/example1', 'SslCommerzPaymentController@exampleEasyCheckout');
Route::get('/example2', 'SslCommerzPaymentController@exampleHostedCheckout');

Route::post('/pay', 'SslCommerzPaymentController@index');

Route::post('/success', 'SslCommerzPaymentController@success');
Route::post('/fail', 'SslCommerzPaymentController@fail');
Route::post('/cancel', 'SslCommerzPaymentController@cancel');

Route::post('/ipn', 'SslCommerzPaymentController@ipn');
//SSLCOMMERZ END


