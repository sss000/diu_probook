<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShareHolderLevel extends Model
{
    protected $guarded = [];

    public function get_users(){
        return $this->hasMany(User::class,'share_holder_level_id');
    }
}
