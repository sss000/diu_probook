<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\ShareHolderLevel;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller
{
    public function index()
    {
        $data = auth()->user();
        $levels = ShareHolderLevel::latest()->first();
        return view('layouts.backend.dashboard',[
            'data'=>$data,
            'levels'=>$levels
        ]);
    }

    public function user_list()
    {
       $users = User::all();
       $data = auth()->user();
       return view('layouts.backend.user.user_list',[
           'users'=>$users,
           'data'=>$data,
       ]);
    }
    public function vendor_list()
    {
       $users = User::all();
       $data = auth()->user();
       return view('layouts.backend.user.vendor_list',[
           'users'=>$users,
           'data'=>$data,
       ]);
    }
}
