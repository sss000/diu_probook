<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShareHolderLevel;
class ShareHolderLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = ShareHolderLevel::latest()->first();

        return view('layouts.backend.dashboard',[
            "levels"=>$levels
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $levels = ShareHolderLevel::latest()->first();
        if($levels != null){
            return response()->json([
                'status'=>'error'
            ],500);
        }else{
            ShareHolderLevel::Create([
                'level'=>$request->level
            ]);

            return response()->json([
                'msg'=>"success"
            ],200);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        ShareHolderLevel::where('id',$request->id)->update([
            'level'=>$request->level
        ]);
        $levels = ShareHolderLevel::latest()->first();
        
        return view('layouts.backend.dashboardTbl',[
            'levels'=>$levels
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
