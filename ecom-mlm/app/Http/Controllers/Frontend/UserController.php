<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use session;
use Illuminate\Support\Facades\Auth;
use App\Models\Settings;

class UserController extends Controller
{
    public function queryForCat(){
        return Category::where('status',1)->with('get_child_category','get_child_category.get_sub_child_category')->get();
    }

    public function index(){
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.user.login',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function registerIndex(){
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.user.register',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function logout(Request $request)
    {
        if(Auth::check()){
            Auth::logout();
            $request->session()->flush();
            
            return redirect()->back();
        }

    }
}
