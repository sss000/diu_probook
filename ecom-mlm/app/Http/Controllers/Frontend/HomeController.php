<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Settings;
use App\Models\ChildCategory;
use App\Models\SubChildCategory;
class HomeController extends Controller
{

    public function queryForCat(){
        return Category::where('status',1)->with('get_child_category','get_child_category.get_sub_child_category')->get();
    }

    public function index()
    {
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.home',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function productByCategory($slug)
    {
        $categories = $this->queryForCat();
        $category = ChildCategory::where('slug',$slug)->with('get_category:id,cover','get_product','get_product.get_product_avatars')->first();
        $sub_category = SubChildCategory::where('slug',$slug)->with('get_category:id,cover','get_product','get_product.get_product_avatars')->first();
        if ($category) {
            $product = $category->get_product()->with('get_brand')->selectRaw('distinct(brand_id)')->get();
            $productColor = $category->get_product()->with('get_attribute_value_id_by_color')->selectRaw('distinct(color)')->get();
            $productSize = $category->get_product()->with('get_attribute_value_id_by_size')->selectRaw('distinct(size)')->get();
        }elseif($sub_category){
            $product = $sub_category->get_product()->with('get_brand')->selectRaw('distinct(brand_id)')->get();
            $productColor = $sub_category->get_product()->with('get_attribute_value_id_by_color')->selectRaw('distinct(color)')->get();
            $productSize = $sub_category->get_product()->with('get_attribute_value_id_by_size')->selectRaw('distinct(size)')->get();
        }

        $setting = Settings::first();

        return view('layouts.frontend.product.category_wise_products',[
            'setting'=>$setting,
            'categories'=>$categories,
            'category'=>$category,
            'sub_category'=>$sub_category,
            'product'=>$product,
            'productColor'=>$productColor,
            'productSize'=>$productSize
        ]);
    }

    public function productDetails()
    {
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.product.product_details',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function viewWishlist(){
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.wishlist.wishlist',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function confirmPayment(){
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.cart.confirm',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function billing_index(){
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.cart.billing_address',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }

    public function viewCart(){
        $categories = $this->queryForCat();
        $setting = Settings::first();

        return view('layouts.frontend.cart.cart',[
            'categories'=>$categories,
            'setting'=>$setting,
        ]);
    }



    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
