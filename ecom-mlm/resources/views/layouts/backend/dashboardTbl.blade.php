<tr id="editRow" style="display: none;">
    <input type="text" id="levelId" name="role" value="" hidden>
    <td id="levelId" class="sorting_1">
        1
    </td>
    <td class="sorting_1">
        <input style="height: 40px !important;
        border: none;
        border-radius: 5px;" type="text" id="levelValue" name="editLevel">
    </td>
    <td style="display: inline-flex">
        <button onclick="updateLevel()" class="btn btn-success">
            <i class="fa fa-check"></i>
        </button>
    </td>
</tr>
<tr id="defaultRow" role="row" class="odd">
    <td class="sorting_1">1</td>
    <td class="sorting_1">{{ optional($levels)->level }}</td>
    <td style="display: inline-flex;">
        <a onclick="editLevel({{ $levels }})" style="margin-right: 5px;" href="#" class="btn btn-primary">
            <i class="fa fa-edit"></i>
        </a>
    </td>
</tr>