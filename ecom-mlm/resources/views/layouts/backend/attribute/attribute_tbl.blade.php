<tr id="editMode" style="display: none;">
    <td id="key"></td>
    <td id="attName"></td>
    <td>
        <input type="hidden" id="slug">
        <input type="text" id="editValue" name="editValue">
    </td>
    <td>
        <button type="button" onclick="updateAttributeValue()" class="btn btn-success btn-sm">
            <i class="fa fa-check"></i>
        </button>
    </td>
</tr>
@foreach ($attribute_values as $key =>$attr)
<tr id="id{{$attr->id}}" role="row" class="odd">
  <td class="sorting_1">{{ $key+1 }}</td>
  <td class="sorting_1">{{$attr->get_attribute->name}}</td>
  <td class="sorting_1">{{$attr->value}}</td>
  <td style="display: inline-flex;">
      <button style="margin-right: 5px;" class="btn btn-primary btn-sm" onclick="showId({{$attr}})">
        <i class="fa fa-edit"></i>
      </button>
    <form action="" method="post">
      @csrf
      <input type="text" name="role" value="user" hidden>
      <button type="submit" class="btn btn-danger btn-sm">
        <i class="fa fa-trash"></i>
      </button>
    </form>
  </td>
</tr>
@endforeach