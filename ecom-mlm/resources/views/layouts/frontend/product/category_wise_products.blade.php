@extends('layouts.frontend.app')


@section('content')
@section('css')
    <link href="{{ asset('assets/css/listing.css') }}" rel="stylesheet">
@endsection

<main>
    <div class="top_banner">
        <div class="opacity-mask d-flex align-items-center" data-opacity-mask="rgba(0, 0, 0, 0.3)">
            <div class="container">
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Category</a></li>
                        <li>Page active</li>
                    </ul>
                </div>
                <h1>Shoes - Grid listing</h1>
            </div>
        </div>
        @php
            if($category !=null){
                $category = $category;
            }elseif($sub_category != null){
                $category = $sub_category;
            }
        @endphp
        <img style="height: 250px !important;" src="/images/{{optional($category->get_category)->cover}}" class="img-fluid" alt="">
    </div>
    <div id="stick_here"></div>
    <div class="container margin_30">
        <div class="row">
            <aside class="col-lg-3" id="sidebar_fixed">
                <div class="filter_col">
                    <div class="inner_bt"><a href="#" class="open_filters"><i class="ti-close"></i></a></div>
                    <div class="filter_type version_2">
                        <h4><a href="#filter_1" data-toggle="collapse" class="opened">Categories</a></h4>
                        <div class="collapse show" id="filter_1">
                            <ul>
                                <li>
                                    <label style="color: green;font-weight: 700;" class="container_check">
                                        {{ optional($category)->child_name }}
                                    </label>
                                    <label style="color: green;font-weight: 700;" class="container_check">
                                        {{ optional($sub_category)->sub_child_name }}
                                    </label>
                                    
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="filter_type version_2">
                        <h4><a href="#filter_3" data-toggle="collapse" class="closed">Brands</a></h4>
                        <div class="collapse" id="filter_3">
                            <ul>
                                @foreach ($product as $pro)
                                <li>
                                    <label class="container_check">{{optional($pro->get_brand)->brand_name}}
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="filter_type version_2">
                        <h4><a href="#filter_2" data-toggle="collapse" class="opened">Color</a></h4>
                        <div class="collapse show" id="filter_2">
                            <ul>
                                @foreach ($productColor as $pro)
                                <li>
                                    <label class="container_check">{{optional($pro->get_attribute_value_id_by_color)->value}}
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="filter_type version_2">
                        <h4><a href="#filter_2" data-toggle="collapse" class="opened">Size</a></h4>
                        <div class="collapse show" id="filter_2">
                            <ul>
                                @foreach ($productSize as $pro)
                                <li>
                                    <label class="container_check">{{optional($pro->get_attribute_value_id_by_size)->value}}
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    
                    <div class="filter_type version_2">
                        <h4><a href="#filter_4" data-toggle="collapse" class="closed">Price</a></h4>
                        <div class="collapse" id="filter_4">
                            <ul>
                                <li>
                                    <label class="container_check">$0 — $50<small>11</small>
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container_check">$50 — $100<small>08</small>
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container_check">$100 — $150<small>05</small>
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                                <li>
                                    <label class="container_check">$150 — $200<small>18</small>
                                        <input type="checkbox">
                                        <span class="checkmark"></span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="buttons">
                        <a href="#0" class="btn_1">Filter</a> <a href="#0" class="btn_1 gray">Reset</a>
                    </div>
                </div>
            </aside>
            @include('layouts.frontend.product.all_products')
        </div> 
    </div> 
   
</main>
<!-- /main -->



@section('js')

<script src="{{ asset('assets/js/specific_listing.js') }}"></script>
<script src="{{ asset('assets/js/sticky_sidebar.js') }}"></script>
<script>

</script>

@endsection
@endsection
