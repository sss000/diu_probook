
    <div class="col-lg-9">
        <div class="row small-gutters" id="products">
            @foreach ($category->get_product as $pro)
                <div class="col-6 col-md-4">
                    <div class="grid_item">
                        @foreach ($pro->get_product_avatars as $avtr)
                            <figure>
                                <a href="product-detail-1.html">
                                <img class="img-fluid lazy" src="/images/{{$avtr->front}}"
                                        data-src="/images/{{$avtr->front}}" alt="">
                                </a>
                                <div data-countdown="2020/03/15" class="countdown"></div>
                            </figure>
                        @endforeach
                        <a href="#">
                            <h3>{{$pro->product_name}}</h3>
                        </a>
                        <div class="price_box">
                            <span class="new_price">{{$pro->sale_price}}</span>
                            <span class="old_price">{{$pro->promo_price}}</span>
                        </div>
                        <ul>
                            <li>
                                <a href="#0" class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                    title="Add to favorites"><i class="ti-heart"></i><span>Add to favorites</span>
                                </a>
                            </li>
                            <li>
                                <a href="#0" class="tooltip-1" data-toggle="tooltip" data-placement="left"
                                    title="Add to cart"><i class="ti-shopping-cart"></i><span>Add to cart</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- /row -->
        <div class="pagination__wrapper">
            <ul class="pagination">
                <li><a href="#0" class="prev" title="previous page">&#10094;</a></li>
                <li>
                    <a href="#0" class="active">1</a>
                </li>
                <li>
                    <a href="#0">2</a>
                </li>
                <li>
                    <a href="#0">3</a>
                </li>
                <li>
                    <a href="#0">4</a>
                </li>
                <li><a href="#0" class="next" title="next page">&#10095;</a></li>
            </ul>
        </div>
    </div>
