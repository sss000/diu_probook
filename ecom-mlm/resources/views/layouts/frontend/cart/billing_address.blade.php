@extends('layouts.frontend.app')


@section('content')
@section('css')
    <link href="{{ asset('assets/css/checkout.css') }}" rel="stylesheet">
@endsection


<main class="bg_gray">


	<div class="container margin_30">
		<div class="page_header">
			<div class="breadcrumbs">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Category</a></li>
					<li>Page active</li>
				</ul>
		    </div>
		    <h1>Sign In or Create an Account</h1>
	    </div>
	<!-- /page_header -->
        <div class="row">

            <div class="col-lg-4 col-md-6">

                <form action="{{ url('/pay') }}" method="POST">
                    <div class="step first">
                        <h3>1. Billing address</h3>
                        <div class="checkout">
                            <div class="form-group">
                                <input type="text" name="customer_name" class="form-control" placeholder="Full Name">
                            </div>
                            <div class="form-group">
                                <input type="text" name="customer_phone" class="form-control" placeholder="Mobile">
                            </div>
                            <div class="form-group">
                                <input type="email" name="customer_email" class="form-control" placeholder="Email">
                            </div>

                            <div class="form-group">
                                <input type="text" name="customer_address" class="form-control" placeholder="Address">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="step middle payments">
                        <h3>2. Payment and Shipping</h3>
                        <ul>
                            <li>
                                <label class="container_radio">Credit Card
                                    <input type="radio" name="payment" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_radio">Cash on delivery
                                    <input type="radio" name="payment">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>

                        <h6 class="pb-2">Shipping Method</h6>
                        <ul>
                            <li>
                                <label class="container_radio">Inside Dhaka
                                    <input type="radio" name="shipping" checked>
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                                <label class="container_radio">Outside Dhaka
                                    <input type="radio" name="shipping">
                                    <span class="checkmark"></span>
                                </label>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="step last">
                        <h3>3. Order Summary</h3>
                        <div class="box_general summary">
                            <ul>
                                <li class="clearfix"><em>1x Armor Air X Fear</em>  <span>$145.00</span></li>
                                <li class="clearfix"><em>2x Armor Air Zoom Alpha</em> <span>$115.00</span></li>
                            </ul>
                            <ul>
                                <li class="clearfix"><em><strong>Subtotal</strong></em>  <span>$450.00</span></li>
                                <li class="clearfix"><em><strong>Shipping</strong></em> <span>$0</span></li>
                            </ul>
                            <div class="total clearfix">TOTAL <span>$450.00</span></div>

                            <button type="submit" class="btn_1 full-width">Confirm and Pay</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
	</div>
	<!-- /container -->
</main>
<!--/main-->


@section('js')


@endsection
@endsection
