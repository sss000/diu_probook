@extends('layouts.frontend.app')


@section('content')
@section('css')
    <link href="{{ asset('assets/css/cart.css') }}" rel="stylesheet">
@endsection

<main class="bg_gray">
    <div class="container margin_30">
    <div class="page_header">
        <div class="breadcrumbs">
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">Category</a></li>
                <li>Page active</li>
            </ul>
        </div>
        <h1>Wishlist page</h1>
    </div>
    <!-- /page_header -->
    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>
                                    Product
                                </th>
                                <th>
                                    Price
                                </th>
                                <th>
                                    Stock Status
                                </th>
                                <th>

                                </th>
                                <th>

                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="thumb_cart">
                                        <img src="assets/img/products/product_placeholder_square_small.jpg" data-src="assets/img/products/shoes/1.jpg" class="lazy" alt="Image">
                                    </div>
                                    <span class="item_cart">Armor Air x Fear</span>
                                </td>
                                <td>
                                    <strong>$140.00</strong>
                                </td>
                                <td>
                                    <span class="item_cart">In Stock</span>
                                </td>
                                <td>
                                    <a href="#" class="btn_1 cartaddbtn">Add to Cart</a>
                                </td>
                                <td class="options">
                                    <a href="#"><i class="ti-trash"></i></a>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div class="thumb_cart">
                                        <img src="assets/img/products/product_placeholder_square_small.jpg" data-src="assets/img/products/shoes/2.jpg" class="lazy" alt="Image">
                                    </div>
                                    <span class="item_cart">Armor Okwahn II</span>
                                </td>
                                <td>
                                    <strong>$140.00</strong>
                                </td>
                                <td>
                                    <span class="item_cart">In Stock</span>
                                </td>
                                <td>
                                    <a href="#" class="btn_1 cartaddbtn">Add to Cart</a>
                                </td>
                                <td class="options">
                                    <a href="#"><i class="ti-trash"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="thumb_cart">
                                        <img src="assets/img/products/product_placeholder_square_small.jpg" data-src="assets/img/products/shoes/3.jpg" class="lazy" alt="Image">
                                    </div>
                                    <span class="item_cart">Armor Air Wildwood ACG</span>
                                </td>
                                <td>
                                    <strong>$140.00</strong>
                                </td>
                                <td>
                                    <span class="item_cart">In Stock</span>
                                </td>
                                <td>
                                    <a href="#" class="btn_1 cartaddbtn">Add to Cart</a>
                                </td>
                                <td class="options">
                                    <a href="#"><i class="ti-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

    </div>
    <!-- /container -->

</main>
<!--/main-->





@section('js')

<script>

</script>

@endsection
@endsection
